VERSION := 1.7
MODEL := pinebook
KVER := 4.4.206
BLOB := https://github.com/mrfixit2001/updates_repo/archive/v$(VERSION).tar.gz
PKGS := base-system linit startup openssh openntpd linux-firmware-network
REPO := https://alpha.de.repo.voidlinux.org/current/aarch64
ARCH := aarch64
QEMU_STATIC := /usr/bin/qemu-aarch64-static

.PHONY: all configure rootfs bootfs clean

all: configure bootfs

configure: rootfs
	sudo cp -rT -- etc "$(ROOTFS)/etc"

rootfs: updates_repo-$(VERSION)
	test "$(ROOTFS)"
	sudo install -d "$(ROOTFS)/var/db/xbps"
	sudo cp -rT keys "$(ROOTFS)/var/db/xbps/keys"
	sudo install -D "$(QEMU_STATIC)" "$(ROOTFS)$(QEMU_STATIC)"
	sudo env "XBPS_TARGET_ARCH=$(ARCH)" xbps-install -r "$(ROOTFS)" -R "$(REPO)" -USy $(PKGS)
	sudo env "XBPS_ARCH=$(ARCH)" xbps-reconfigure -r "$(ROOTFS)" -f base-files
	sudo env "XBPS_ARCH=$(ARCH)" chroot "$(ROOTFS)" xbps-reconfigure -af
	sudo rm -f -- "$(ROOTFS)$(QEMU_STATIC)"
	sudo install -d "$(ROOTFS)/usr/lib/modules"
	sudo cp -rT "$</$(MODEL)/filesystem/$(KVER)" "$(ROOTFS)/usr/lib/modules/$(KVER)"

bootfs: updates_repo-$(VERSION)
	test "$(BOOTFS)"
	sudo install -t "$(BOOTFS)" "$</$(MODEL)/kernel/Image" "$</$(MODEL)/kernel/"*.dtb
	sudo install -D 'extlinux.conf' "$(BOOTFS)/extlinux/extlinux.conf"

disk: updates_repo-$(VERSION)
	test "$(TARGET)"
	sudo sfdisk "$(TARGET)" < gpt.sfdisk
	sudo dd if="$</$(MODEL)/filesystem/idbloader.img" of="$(TARGET)p1"
	sudo dd if="$</$(MODEL)/filesystem/uboot.img" of="$(TARGET)p2"
	sudo dd if="$</$(MODEL)/filesystem/trust.img" of="$(TARGET)p3"
	sudo mkfs.vfat -n EFI -F 32 "$(TARGET)p4"
	sudo mkfs.ext4 -FL Boot "$(TARGET)p5"
	sudo mkfs.ext4 -FL RootFS "$(TARGET)p6"

updates_repo-$(VERSION): updates_repo-$(VERSION).tar.gz
	tar xf "$<"

updates_repo-$(VERSION).tar.gz:
	xbps-fetch -o "$@" "$(BLOB)"

clean:
	rm -rf -- "updates_repo-$(VERSION)"
	rm -f -- "updates_repo-$(VERSION).tar.gz"
